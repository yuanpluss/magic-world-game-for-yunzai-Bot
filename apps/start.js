import plugin from '../../../lib/plugins/plugin.js'
import {getDirs} from '../component/getDies.js'
import {personality,talent} from '../component/random.js'
import {experience} from '../component/exp.js'
import fs from "fs"
import path from 'path';
const  _path = process.cwd()
let dirpath = _path + '/data/YTgame/group';
if (!fs.existsSync(dirpath)) {
    fs.mkdirSync(dirpath);
  }
  import puppeteer from '../../../lib/puppeteer/puppeteer.js'
export class example extends plugin {
  constructor() {
    super({
      /** 功能名称 */
      name: '阴天',
      /** 功能描述 */
      dsc: 'start',
      /** https://oicqjs.github.io/oicq/#events */
      event: 'message',
      /** 优先级，数字越小等级越高 */
      priority: 775,
      rule: [
        {
          /** 命令正则匹配 */
          reg: '^/开启本群$',
          /** 执行方法 */
          fnc: 'infor'
        },
        {
          /** 命令正则匹配 */
          reg: '^/创建角色$',
          /** 执行方法 */
          fnc: 'getUser'
        },
        {
          /** 命令正则匹配 */
          reg: '^/选择宠物(喵喵|火花|水蓝蓝)$',
          /** 执行方法 */
          fnc: 'getPet'
        }
      ]
    })
}
async infor(e){
if(!e.isMaster){e.reply("仅主人可开启");return false}
if(!e.isGroup){e.reply("仅群聊可开启");return false}
let groupID = `${e.group_id}`
let dirNames = await getDirs(dirpath,false)
if(dirNames.includes(groupID))
  {
    e.reply("本群早就已经开启过阴天game了哦！")
    return false 
  }
else
  {
    try{
      fs.mkdirSync(`${dirpath}/${groupID}`);
    e.reply("开启成功，本群可以使用阴天游戏功能了！")
    return true }catch(err){
      e.reply("开启失败，原因：\n"+err)
      return false
    }
  }
}
async getUser(e){
  if(!e.isGroup){e.reply("仅群聊可使用");return false}
  let groupID = `${e.group_id}`
console.log(groupID)
  let dirNames = await getDirs(dirpath,false)
  console.log(dirNames)
  if(!dirNames.includes(groupID))
  {
    e.reply("主人尚未在本群开启游戏，无法创建游戏角色档案。")
    return false 
  }
  let userID = `${e.user_id}`
  let userNames = await getDirs(`${dirpath}/${groupID}`,false)
  if(userNames.includes(userID))
  {
    e.reply("你在此群早就已经有了游戏角色档案了哦")
    return false 
  }
else
  {
      try{

        let currentTime = new Date();
        let year = currentTime.getFullYear().toString(); // 获取当前年份并转换为字符串
        let month = (currentTime.getMonth() + 1).toString().padStart(2, '0'); // 获取当前月份并转换为字符串，并补零
        let day = currentTime.getDate().toString().padStart(2, '0'); // 获取当前日期并转换为字符串，并补零
        let jointime = `${year}年${month}月${day}日`;
        fs.writeFileSync(`${dirpath}/${groupID}/${userID}.json`,JSON.stringify({
          "user":`${e.sender.nickname}`,
          "jointime":jointime,
          "money":0,
          "level":1,
        }));
      e.reply("创建角色档案成功，请你选择一只初始宠物，火系：火花，草系：喵喵，水系：水蓝蓝。如：/选择宠物 火花。")
      return true
         }catch(err){
      e.reply("创建角色失败，原因：\n"+err)
      return false
               }
  }
}
async getPet(e)
{ 
  let msg = e.msg.replace("/选择宠物","").trim()
  if(!e.isGroup){e.reply("仅群聊可使用");return false}
  let groupID = `${e.group_id}`
  let dirNames = await getDirs(dirpath,false)
  if(!dirNames.includes(groupID))
  {
    e.reply("主人尚未在本群开启游戏，无法操作。")
    return false 
  }
  let userID = `${e.user_id}`
  let userNames = await getDirs(`${dirpath}/${groupID}`,false)
  if(!userNames.includes(userID))
  {
    e.reply("你尚未创建角色，请发送：/创建角色 ")
    return false 
  }
  let data = fs.readFileSync(`${dirpath}/${groupID}/${userID}.json`)
  let obj = JSON.parse(data)
  if( "pet1" in obj){e.reply("你已经领取过一个宠物伙伴了，不可以重复领取哦！不然它会伤心的。");return false}
  let petData = fs.readFileSync(_path +`/plugins/magic-world-game-plugin/model/pet/pet.json`)
  let petObj = JSON.parse(petData)
  let talent1 = await talent()
  let result = await personality(petObj[`${msg}`])
  let HP = Math.round(result[1].HP * (0.08 * talent1.talent.HP + 1));
  let ATK = Math.round(result[1].ATK * (0.05 * talent1.talent.ATK + 1));
  let DEF = Math.round(result[1].DEF * (0.05 * talent1.talent.DEF + 1));
  let MATK = Math.round(result[1].MATK * (0.05 * talent1.talent.MATK + 1));
  let MDEF = Math.round(result[1].MDEF * (0.05 * talent1.talent.MDEF + 1));
  let SPEED = Math.round(result[1].SPEED * (0.05 * talent1.talent.SPEED + 1));
  let needExp = await experience(1,1)
  obj.pet1= {
    [msg]:{
      "ELEMENT": petObj[`${msg}`].ELEMENT,
      "level":1,
      "HP":HP,
      "ATK":ATK,
      "DEF":DEF,
      "MATK":MATK,
      "MDEF":MDEF,
      "SPEED":SPEED,
      "talent":talent1.talent,
      "personality":result[0],
      "EXP":1,
      "needEXP":needExp[1],
      "realneedEXP":needExp[0]
    }
  }
 
  fs.writeFileSync(`${dirpath}/${groupID}/${userID}.json`,JSON.stringify(obj))
  e.reply(`恭喜你获得了即将与你一路同行的宠物伙伴：${msg},性格：${obj.pet1[msg].personality},当前天赋:精力：${talent1.talent.HP},攻击：${talent1.talent.ATK}，物防：${talent1.talent.DEF}，魔攻：${talent1.talent.MATK}，魔抗：${talent1.talent.MDEF},速度：${talent1.talent.SPEED}。`)
let data2 = {
              tplFile: _path + "/plugins/resources/html/choose.html",
  "level":1,
      "HP":HP,
      "ATK":ATK,
      "DEF":DEF,
      "MATK":MATK,
      "MDEF":MDEF,
      "SPEED":SPEED,
     			
name:msg,
xg:obj.pet1[msg].personality,
atk:talent1.talent.ATK,
hp:talent1.talent.HP,
def:talent1.talent.DEF,
matk:talent1.talent.MATK,
mdef:talent1.talent.MDEF,
speed:talent1.talent.SPEED,
}
            let img = await puppeteer.screenshot("777", {
              ...data2,
            });
  e.reply(img)
}
}

