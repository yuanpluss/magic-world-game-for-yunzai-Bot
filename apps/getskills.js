import plugin from '../../../lib/plugins/plugin.js'
import {getDirs} from '../component/getDies.js'
import { start } from '../component/isOPEN.js';
import common from'../../../lib/common/common.js'
import fs from "fs"
import path from 'path';
const  _path = process.cwd()
let dirpath = _path + '/data/YTgame/group';
if (!fs.existsSync(dirpath)) {
    fs.mkdirSync(dirpath);
  }
let replaceStart = false
let replaceQQ = ""
let replaceGroup = ""
let replaceName = ""
let petnum = ""
export class example extends plugin {
  constructor() {
    super({
      name: '阴天',
      dsc: 'getskill',
      event: 'message',
      priority: 775,
      rule: [
        {
          /** 命令正则匹配 */
          reg: '^/查看可学技能(.*)$',
          /** 执行方法 */
          fnc: 'canget'
        },{
            /** 命令正则匹配 */
            reg: /\/(.*)学习技能(.*)/,
            /** 执行方法 */
            fnc: 'getskill'
          },
          {
            /** 命令正则匹配 */
            reg: '^/替换技能(.*)$',
            /** 执行方法 */
            fnc: 'replaceskill'
          },
        
       
      ]
    })
}
async canget(e){
    let isOpen = await start(e)
    if(!isOpen){return false}
    let groupID = `${e.group_id}`
    let userID = `${e.user_id}`
    let data = fs.readFileSync(`${dirpath}/${groupID}/${userID}.json`)
    let obj = JSON.parse(data)
    let skillData = fs.readFileSync(_path +`/plugins/magic-world-game-plugin/model/skill/skills.json`)
    let skillObj = JSON.parse(skillData)
    let msg = e.msg.replace("/查看可学技能","").trim()
    let showCanGet = []
    let a = true
    for(let key in obj)
    {  
        if (key == "pet1" || key == "pet2" || key == "pet3" || key == "pet4" || key == "pet5" || key == "pet6") {
            // 检测是否有开启技能格子的字段，如果没有则自动开启
            if (!obj[key].skills) {
              obj[key].skills = {};
              obj[key].skills.skill1 = null;
              obj[key].skills.skill2 = null;
              obj[key].skills.skill3 = null;
              obj[key].skills.skill4 = null;
              // 添加其他技能格子的初始化
            }
            if (msg in obj[key]) {
              a = false
              for (let key1 in skillObj) {
                if (key1 == obj[key][msg].ELEMENT) {
                  for (let i = 0; i < skillObj[key1].length; i++) {
                    if ((skillObj[key1][i].whoCanGet == "All" || skillObj[key1][i].whoCanGet.includes(msg)) && Number(skillObj[key1][i].needLevel) <= Number(obj[key][msg].level)) {
                      // 检测是否已经学过该技能，如果学过则跳过
                      if (!(skillObj[key1][i].skillName in obj[key].skills)) {
                        showCanGet.push(skillObj[key1][i].skillName + ":" + skillObj[key1][i].技能介绍 + `\n威力:${skillObj[key1][i].威力}`)
                      }
                    }
                  }
                }
              }
            }
          }
    }
    if(showCanGet.length!==0)
    {
        e.reply(`你的宠物：${msg}\n当前可学技能:\n${showCanGet.join("\n")}`)
    }else if(a)
    {
        e.reply(`找不到宠物：${msg},请确保其输入正确并且放在你的背包里。`)
    }else
    {
        e.reply(`你的宠物：${msg},当前无可学新技能。`)
    }

}
async getskill(e){
    let isOpen = await start(e)
    console.log(isOpen)
    if(!isOpen){return false}
    if(replaceStart){e.reply(`有玩家正在替换技能，请稍后再试。。`)
    return false}
    let msg = e.msg.trim()
    let groupID = `${e.group_id}`
    let userID = `${e.user_id}`
    let matches = msg.match(/\/(.*)学习技能(.*)/);
    let pet = matches[1].trim();
    let skill = matches[2].trim();
    let data = fs.readFileSync(`${dirpath}/${groupID}/${userID}.json`)
    let obj = JSON.parse(data)
    let skillData = fs.readFileSync(_path +`/plugins/magic-world-game-plugin/model/skill/skills.json`)
    let skillObj = JSON.parse(skillData)
    let showCanGet = []
    let a = true
    for(let key in obj)
    {  
        if (key == "pet1" || key == "pet2" || key == "pet3" || key == "pet4" || key == "pet5" || key == "pet6") {
            // 检测是否有开启技能格子的字段，如果没有则自动开启
            if (!obj[key].skills) {
              obj[key].skills = {};
              obj[key].skills.skill1 = null;
              obj[key].skills.skill2 = null;
              obj[key].skills.skill3 = null;
              obj[key].skills.skill4 = null;
              // 添加其他技能格子的初始化
            }
            if (pet in obj[key]) {
              a = false
              for (let key1 in skillObj) {
                if (key1 == obj[key][pet].ELEMENT) {
                  for (let i = 0; i < skillObj[key1].length; i++) {
                    if ((skillObj[key1][i].whoCanGet == "All" || skillObj[key1][i].whoCanGet.includes(pet)) && Number(skillObj[key1][i].needLevel) <= Number(obj[key][pet].level)) {
                      // 检测是否已经学过该技能，如果学过则跳过
                      if (!(skillObj[key1][i].skillName in obj[key].skills)) {
                        showCanGet.push(skillObj[key1][i].skillName)              
                      }
                    }
                  }
                }
              }
            }
          }
    }
    if(showCanGet.length!==0)
    {
        if(!showCanGet.includes(skill)){e.reply("请你检查输入技能是否在可学技能列表里");return}
        else{
            for(let key in obj)
            {  
                if (key == "pet1" || key == "pet2" || key == "pet3" || key == "pet4" || key == "pet5" || key == "pet6") {
                    if (
                        obj[key].skills.skill1 === null ||
                        obj[key].skills.skill2 === null ||
                        obj[key].skills.skill3 === null ||
                        obj[key].skills.skill4 === null
                      ) {
                        // 选出为 null 的数字比较靠前的技能格子并替换
                        if (obj[key].skills.skill1 === null) {
                          obj[key].skills.skill1 = skill;
                        } else if (obj[key].skills.skill2 === null) {
                          obj[key].skills.skill2 = skill;
                        } else if (obj[key].skills.skill3 === null) {
                          obj[key].skills.skill3 = skill;
                        } else if (obj[key].skills.skill4 === null) {
                          obj[key].skills.skill4 = skill;
                        }
                        fs.writeFileSync(`${dirpath}/${groupID}/${userID}.json`, JSON.stringify(obj));
                        e.reply("学习成功")
                      }else
                      {
                         replaceStart = true
                         replaceQQ = `${e.user_id}`
                         replaceGroup = `${e.group_id}`
                         replaceName = `${skill}`
                         petnum = `${key}`
                         e.reply(`你的宠物${pet}技能槽已满，请选择一个技能替换：\n技能1:${obj[key].skills.skill1}\n技能2:${obj[key].skills.skill2}\n技能3:${obj[key].skills.skill3}\n技能4:${obj[key].skills.skill4}\n指令示例:/替换技能1\n替换将在30s内失效,请尽快操作。`)
                         await common.sleep(30000)
                         if( replaceStart != false&&
                          replaceQQ !== ""&&
                          replaceGroup !== ""&&
                          replaceName !== ""&&
                          petnum !== ""){
                         replaceStart = false
                         replaceQQ = ""
                         replaceGroup = ""
                         replaceName = ""
                         petnum = ""}
                      }
                  }
            }
        }
        
    }else if(a)
    {
        e.reply(`找不到宠物：${pet},请确保其输入正确并且放在你的背包里。`)
    }else
    {
        e.reply(`你的宠物：${pet}学不了一点，请检查你的技能名输入是否有问题。`)
    }
}
async replaceskill(e){
    let isOpen = await start(e)
    console.log(isOpen)
    if(!isOpen){return false}
    if(!replaceStart){return false}
    let groupID = `${e.group_id}`
    let userID = `${e.user_id}`
    if(replaceGroup!==groupID||replaceQQ!==userID){return false}
    let data = fs.readFileSync(`${dirpath}/${groupID}/${userID}.json`)
    let obj = JSON.parse(data)
    let skillIndex = parseInt(e.msg.replace("/替换技能", "").trim());
    if (skillIndex < 1 || skillIndex > 4) {
      e.reply("请输入有效的技能槽编号（1-4）");
      return false;
    }
         
            if (skillIndex === 1) {
                obj[petnum].skills.skill1 = replaceName;
            } else if (skillIndex === 2) {
                obj[petnum].skills.skill2 = replaceName;
            } else if (skillIndex === 3) {
                obj[petnum].skills.skill3 = replaceName;
            } else if (skillIndex === 4) {
                obj[petnum].skills.skill4 = replaceName;
            }
            fs.writeFileSync(`${dirpath}/${groupID}/${userID}.json`, JSON.stringify(obj));
e.reply("替换成功")
                         replaceStart = false
                         replaceQQ = ""
                         replaceGroup = ""
                         replaceName = ""
                         petnum = ""
        }
}