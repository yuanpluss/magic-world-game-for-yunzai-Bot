import plugin from '../../../lib/plugins/plugin.js'
import fs from "fs"
import {getDirs} from '../component/getDies.js'
import {afterEVO} from '../component/random.js'
const  _path = process.cwd()
let dirpath = _path + '/data/YTgame/group';
if (!fs.existsSync(dirpath)) {
    fs.mkdirSync(dirpath);
  }
  export class example extends plugin {
    constructor() {
      super({
        name: '阴天',
        dsc: 'EVO',
        event: 'message',
        priority: 775,
        rule: [
          {
            reg: "/宠物进化(.*)",
            fnc: 'evo'
          }
        ]
      })
  }
  async evo(e){
    if(!e.isGroup){e.reply("仅群聊可用");return false}
    let groupID = `${e.group_id}`
    let dirNames = await getDirs(dirpath,false)
    if(!dirNames.includes(groupID)){
        e.reply("本群尚未开启阴天game")
        return false
    }
    let userID = `${e.user_id}`
    let userNames = await getDirs(`${dirpath}/${groupID}`,false)
    if(!userNames.includes(userID))
    {
      e.reply("你尚未在此群创建游戏角色档案")
      return false 
    }
    let data = fs.readFileSync(`${dirpath}/${groupID}/${userID}.json`)
    let obj = JSON.parse(data)
    let petData = fs.readFileSync(_path +`/plugins/magic-world-game-plugin/model/pet/pet.json`)
    let petObj = JSON.parse(petData)
    let msg = e.msg.replace("/宠物进化","").trim()
    let isCanEvo = false
    for(let key in obj)
    {
        if(key =="pet1"||key =="pet2"||key =="pet3"||key =="pet4"||key =="pet5"||key =="pet6")
        {
            if(msg in obj[key])
            {
               if(Number(obj[key][msg].level)>Number(petObj[msg].isEVO)&&petObj[msg].isEVO!==null&&petObj[msg].NEXT!==null)
               {
                    
                    obj[key][petObj[msg].NEXT]=obj[key][msg]
                    delete obj[key][msg]
                    let result = await afterEVO(petObj[petObj[msg].NEXT], obj[key][petObj[msg].NEXT].personality)
                    obj[key][petObj[msg].NEXT].HP = Math.round(Math.round(result.HP * (0.08 * obj[key][petObj[msg].NEXT].talent.HP + 1))*(0.045*(obj[key][petObj[msg].NEXT].level-1)+1));
                    obj[key][petObj[msg].NEXT].ATK = Math.round(Math.round(result.ATK * (0.05 * obj[key][petObj[msg].NEXT].talent.ATK + 1))*(0.045*(obj[key][petObj[msg].NEXT].level-1)+1));
                    obj[key][petObj[msg].NEXT].DEF = Math.round(Math.round(result.DEF * (0.05 * obj[key][petObj[msg].NEXT].talent.DEF + 1))*(0.045*(obj[key][petObj[msg].NEXT].level-1)+1));
                    obj[key][petObj[msg].NEXT].MATK = Math.round(Math.round(result.MATK * (0.05 * obj[key][petObj[msg].NEXT].talent.MATK + 1))*(0.045*(obj[key][petObj[msg].NEXT].level-1)+1));
                    obj[key][petObj[msg].NEXT].MDEF = Math.round(Math.round(result.MDEF * (0.05 * obj[key][petObj[msg].NEXT].talent.MDEF + 1))*(0.045*(obj[key][petObj[msg].NEXT].level-1)+1));
                    obj[key][petObj[msg].NEXT].SPEED = Math.round(Math.round(result.SPEED * (0.05 * obj[key][petObj[msg].NEXT].talent.SPEED + 1))*(0.045*(obj[key][petObj[msg].NEXT].level-1)+1));
                    fs.writeFileSync(`${dirpath}/${groupID}/${userID}.json`,JSON.stringify(obj))
                    e.reply(`您的宠物${msg}在此刻爆发出了强大的力量，外形发生了巨大变化，进化成为：${petObj[msg].NEXT}，似乎变得更加强大了！`)
                    isCanEvo = true
                    break    
               }
            }
        }
    }
    if(!isCanEvo)
    {
        e.reply("无法完成进化，请确认你输入的宠物名字存在且放入你的宠物背包内并且满足进化条件,或者是该宠物已经是最高形态无法进化。")
        return false
    }


  }}