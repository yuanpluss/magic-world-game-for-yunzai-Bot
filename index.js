import fs from 'node:fs'
import chalk from 'chalk'
const _path = process.cwd()
let dirpath = _path + '/data/YTgame/';
if (!fs.existsSync(dirpath)) {
    fs.mkdirSync(dirpath);
  }
const files = fs.readdirSync('./plugins/magic-world-game-plugin/apps').filter(file => file.endsWith('.js'))

let ret = []

logger.info(chalk.rgb(50, 240, 108)(`---~~~~·❤·~~~~---`))
logger.info(chalk.rgb(50, 240, 108)(`Magic-world-game-plugin`))
logger.info(chalk.rgb(50, 240, 108)(`贡献者：`))
logger.info(chalk.rgb(50, 240, 108)(`鸢 & 天球生物`))
logger.info(chalk.rgb(50, 240, 108)(`---------------------`));
 
files.forEach((file) => {
  ret.push(import(`./apps/${file}`))
})

ret = await Promise.allSettled(ret)

let apps = {}
for (let i in files) {
  let name = files[i].replace('.js', '')

  if (ret[i].status != 'fulfilled') {
    logger.error(`载入插件错误：${logger.red(name)}`)
    logger.error(ret[i].reason)
    continue
  }
  apps[name] = ret[i].value[Object.keys(ret[i].value)[0]]
}

export { apps }