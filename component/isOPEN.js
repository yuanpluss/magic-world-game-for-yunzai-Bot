import {getDirs} from './getDies.js'
import fs from "fs"
const  _path = process.cwd()
let dirpath = _path + '/data/YTgame/group';
if (!fs.existsSync(dirpath)) {
    fs.mkdirSync(dirpath);
  }
export async function start(e){
    if(!e.isGroup){e.reply("仅群聊可用");return false}
    let groupID = `${e.group_id}`
    let dirNames = await getDirs(dirpath,false)
    if(!dirNames.includes(groupID)){
        e.reply("本群尚未开启阴天game")
        return false
    }
    let userID = `${e.user_id}`
    let userNames = await getDirs(`${dirpath}/${groupID}`,false)
    if(!userNames.includes(userID))
    {
      e.reply("你尚未在此群创建游戏角色档案")
      return false 
    }
    return true
  }