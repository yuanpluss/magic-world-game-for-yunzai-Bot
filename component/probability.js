export async function gailv(num) {
  let num = Math.floor(Math.random() * num) + 1
  let nums = new Set()
  while (nums.has(num)) {
    num = Math.floor(Math.random() * num) + 1
  }
  nums.add(num)
  if (nums.size === num) nums.clear()
  return num
}