import fs from 'fs';
import path from 'path';

export async function getDirs(dir) {
  try {
    let dirents = await fs.promises.readdir(dir, { withFileTypes: true });
    let directories = [];
    for (let dirent of dirents) {
      if (dirent.isDirectory()) {
        directories.push(dirent.name);
      } else {
        let filename = path.parse(dirent.name).name;
        directories.push(filename);
      }
    }
    return directories;
  } catch (error) {
    console.error('Error occurred while reading directories:', error);
    return [];
  }
}