import plugin from '../../../lib/plugins//plugin.js'
import fs from "fs"
import path from 'path';

const  _path = process.cwd()
let dirpath = _path + '/data/YTgame/group';
//petX:第几个宠物，如pet1；petName:宠物名字；getEXP：得到了多少经验；
//groupID和userID字面意思
 export async function levelUp(e,groupID,userID,petX,petName,getEXP){

    let data = fs.readFileSync(`${dirpath}/${groupID}/${userID}.json`)
    let obj = JSON.parse(data)
    if(Number(getEXP)<Number(obj[petX][petName].realneedEXP)){

        let now = obj[petX][petName].realneedEXP - getEXP
        obj[petX][petName].realneedEXP = now
        obj[petX][petName].EXP = obj[petX][petName].needEXP-now
        fs.writeFileSync(`${dirpath}/${groupID}/${userID}.json`,JSON.stringify(obj))
    e.reply(`您的宠物${petName}获得了${getEXP}经验值,好像更喜欢你了呢！距离升级还差：${now}经验值。`)
     }else{
        let note = obj[petX][petName].level
       async function calculateArray(num) {
            // 将数平均分成99份
            const average = num / 99;
            const arr = Array(99).fill(average);
            let a;
            let b = 99;
            for (let i = 0; i < arr.length; i++) {
              a = arr[i];
              if (i !== 98) {
                arr[i] = arr[i] / b;
                a = arr[i];
                arr[i + 1] = arr[i + 1] + a;
              }
              b--;
            }
          
            // 转换为整数
            for (let i = 0; i < arr.length; i++) {
              arr[i] = Math.floor(arr[i]);
            }
          
            return arr;
          }
          
          // 测试
          let num = 8000000; // 要分割的数
          let result = await calculateArray(num);
          let now = Number(getEXP) + Number(obj[petX][petName].EXP)
          let a = false
          if(Number(obj[petX][petName].level) == 100){e.reply(`您的${petName}已经满级了，无法再获得经验了`);a = true ; return}
          
         while(now>Number(obj[petX][petName].needEXP))
         {
            if(Number(obj[petX][petName].level) == 100){e.reply(`您的${petName}已经满级了，无法再获得经验了`);a = true ; break}
            obj[petX][petName].level = Number(obj[petX][petName].level)+1
            obj[petX][petName].needEXP = Number(obj[petX][petName].needEXP)+Number(result[obj[petX][petName].level-1])
         }
         if(a){return}
         obj[petX][petName].realneedEXP = obj[petX][petName].needEXP-now
         obj[petX][petName].needEXP = result[obj[petX][petName].level-1]
         obj[petX][petName].EXP = obj[petX][petName].needEXP-obj[petX][petName].realneedEXP
         obj[petX][petName].HP = Math.round(obj[petX][petName].HP*(0.045*(obj[petX][petName].level-note)+1))
         obj[petX][petName].ATK = Math.round(obj[petX][petName].ATK*(0.045*(obj[petX][petName].level-note)+1))
         obj[petX][petName].DEF = Math.round(obj[petX][petName].DEF*(0.045*(obj[petX][petName].level-note)+1))
         obj[petX][petName].MATK = Math.round(obj[petX][petName].MATK*(0.045*(obj[petX][petName].level-note)+1))
         obj[petX][petName].MDEF = Math.round(obj[petX][petName].MDEF*(0.045*(obj[petX][petName].level-note)+1))
         obj[petX][petName].SPEED = Math.round(obj[petX][petName].SPEED*(0.045*(obj[petX][petName].level-note)+1))

         fs.writeFileSync(`${dirpath}/${groupID}/${userID}.json`,JSON.stringify(obj))
         e.reply(`您的宠物${petName}获得了${getEXP}经验值,升至了${obj[petX][petName].level}级，各方面都提升了,好像更喜欢你了呢！`)
      

     }

 }