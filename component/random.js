export async function personality(base){
    let xingge = ["孤僻", "固执", "调皮", "勇敢", "大胆", "淘气", "无虑", "悠闲", "保守", "稳重", "马虎", "冷静", "沉着", "温顺", "慎重", "狂妄", "胆小", "急躁", "开朗", "天真", "坦率", "害羞", "认真", "实干", "浮躁"]
    let sortedStats = {};
    let all = []
    let randomXingge = xingge[Math.floor(Math.random() * xingge.length)];
    all.push(randomXingge)
    if (randomXingge === "孤僻") {
        sortedStats = {
          "ATK": base.ATK + (base.ATK * 0.1),
          "DEF": base.DEF - (base.DEF * 0.1),
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF,
          "SPEED": base.SPEED
        };
      } else if (randomXingge === "固执") {
        sortedStats = {
          "ATK": base.ATK + (base.ATK * 0.1),
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK - (base.MATK * 0.1),
          "MDEF": base.MDEF,
          "SPEED": base.SPEED
        };
      } else if (randomXingge === "调皮") {
        sortedStats = {
          "ATK": base.ATK + (base.ATK * 0.1),
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF - (base.MDEF * 0.1),
          "SPEED": base.SPEED
        }
      }else if (randomXingge === "勇敢") {
        sortedStats = {
          "ATK": base.ATK + (base.ATK * 0.1),
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF,
          "SPEED": base.SPEED - (base.SPEED * 0.1)
        }
      }else if (randomXingge === "大胆") {
        sortedStats = {
          "ATK": base.ATK - (base.ATK * 0.1),
          "DEF": base.DEF + (base.DEF * 0.1),
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF,
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "淘气") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF + (base.DEF * 0.1),
          "HP": base.HP,
          "MATK": base.MATK- (base.MATK * 0.1),
          "MDEF": base.MDEF,
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "无虑") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF + (base.DEF * 0.1),
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF- (base.MDEF * 0.1),
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "悠闲") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF + (base.DEF * 0.1),
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF,
          "SPEED": base.SPEED- (base.SPEED * 0.1) 
        }
      }else if (randomXingge === "保守") {
        sortedStats = {
          "ATK": base.ATK - (base.ATK * 0.1),
          "DEF": base.DEF ,
          "HP": base.HP,
          "MATK": base.MATK+ (base.MATK * 0.1),
          "MDEF": base.MDEF,
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "稳重") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF- (base.DEF * 0.1) ,
          "HP": base.HP,
          "MATK": base.MATK+ (base.MATK * 0.1),
          "MDEF": base.MDEF,
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "马虎") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK+ (base.MATK * 0.1),
          "MDEF": base.MDEF- (base.MDEF * 0.1) ,
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "冷静") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK+ (base.MATK * 0.1),
          "MDEF": base.MDEF,
          "SPEED": base.SPEED - (base.SPEED * 0.1)
        }
      }else if (randomXingge === "沉着") {
        sortedStats = {
          "ATK": base.ATK- (base.ATK * 0.1) ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF+ (base.MDEF * 0.1),
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "温顺") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF- (base.DEF * 0.1),
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF+ (base.MDEF * 0.1),
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "慎重") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK- (base.MATK * 0.1),
          "MDEF": base.MDEF+ (base.MDEF * 0.1),
          "SPEED": base.SPEED 
        }
      }else if (randomXingge === "狂妄") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF+ (base.MDEF * 0.1),
          "SPEED": base.SPEED- (base.SPEED * 0.1) 
        }
      }else if (randomXingge === "胆小") {
        sortedStats = {
          "ATK": base.ATK- (base.ATK * 0.1) ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF,
          "SPEED": base.SPEED+ (base.SPEED * 0.1) 
        }
      }else if (randomXingge === "急躁") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF- (base.DEF * 0.1),
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF,
          "SPEED": base.SPEED+ (base.SPEED * 0.1) 
        }
      }else if (randomXingge === "开朗") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK- (base.MATK * 0.1),
          "MDEF": base.MDEF,
          "SPEED": base.SPEED+ (base.SPEED * 0.1) 
        }
      }else if (randomXingge === "天真") {
        sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF- (base.MDEF * 0.1),
          "SPEED": base.SPEED+ (base.SPEED * 0.1) 
        }
      }else{
        sortedStats = {
            "ATK": base.ATK ,
            "DEF": base.DEF,
            "HP": base.HP,
            "MATK": base.MATK,
            "MDEF": base.MDEF,
            "SPEED": base.SPEED
          }
      }
      all.push(sortedStats)
      return all;
}

export async function talent(){
    let talent = {
        "talent": {}
      };
      
      let totalPoints = 30;
      
      let attributes = ["HP", "ATK", "DEF", "SPEED", "MATK", "MDEF"];
      
      for (let attribute of attributes) {
        let randomPoints = Math.floor(Math.random() * (totalPoints + 1));
        talent.talent[attribute] = randomPoints;
        totalPoints -= randomPoints;
      }
      return talent

}

export async function afterEVO(base,randomXingge){
    let sortedStats = {};
    if (randomXingge === "孤僻") {
      sortedStats = {
        "ATK": base.ATK + (base.ATK * 0.1),
        "DEF": base.DEF - (base.DEF * 0.1),
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF,
        "SPEED": base.SPEED
      };
    } else if (randomXingge === "固执") {
      sortedStats = {
        "ATK": base.ATK + (base.ATK * 0.1),
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK - (base.MATK * 0.1),
        "MDEF": base.MDEF,
        "SPEED": base.SPEED
      };
    } else if (randomXingge === "调皮") {
      sortedStats = {
        "ATK": base.ATK + (base.ATK * 0.1),
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF - (base.MDEF * 0.1),
        "SPEED": base.SPEED
      }
    }else if (randomXingge === "勇敢") {
      sortedStats = {
        "ATK": base.ATK + (base.ATK * 0.1),
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF,
        "SPEED": base.SPEED - (base.SPEED * 0.1)
      }
    }else if (randomXingge === "大胆") {
      sortedStats = {
        "ATK": base.ATK - (base.ATK * 0.1),
        "DEF": base.DEF + (base.DEF * 0.1),
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF,
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "淘气") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF + (base.DEF * 0.1),
        "HP": base.HP,
        "MATK": base.MATK- (base.MATK * 0.1),
        "MDEF": base.MDEF,
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "无虑") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF + (base.DEF * 0.1),
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF- (base.MDEF * 0.1),
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "悠闲") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF + (base.DEF * 0.1),
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF,
        "SPEED": base.SPEED- (base.SPEED * 0.1) 
      }
    }else if (randomXingge === "保守") {
      sortedStats = {
        "ATK": base.ATK - (base.ATK * 0.1),
        "DEF": base.DEF ,
        "HP": base.HP,
        "MATK": base.MATK+ (base.MATK * 0.1),
        "MDEF": base.MDEF,
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "稳重") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF- (base.DEF * 0.1) ,
        "HP": base.HP,
        "MATK": base.MATK+ (base.MATK * 0.1),
        "MDEF": base.MDEF,
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "马虎") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK+ (base.MATK * 0.1),
        "MDEF": base.MDEF- (base.MDEF * 0.1) ,
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "冷静") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK+ (base.MATK * 0.1),
        "MDEF": base.MDEF,
        "SPEED": base.SPEED - (base.SPEED * 0.1)
      }
    }else if (randomXingge === "沉着") {
      sortedStats = {
        "ATK": base.ATK- (base.ATK * 0.1) ,
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF+ (base.MDEF * 0.1),
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "温顺") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF- (base.DEF * 0.1),
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF+ (base.MDEF * 0.1),
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "慎重") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK- (base.MATK * 0.1),
        "MDEF": base.MDEF+ (base.MDEF * 0.1),
        "SPEED": base.SPEED 
      }
    }else if (randomXingge === "狂妄") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF+ (base.MDEF * 0.1),
        "SPEED": base.SPEED- (base.SPEED * 0.1) 
      }
    }else if (randomXingge === "胆小") {
      sortedStats = {
        "ATK": base.ATK- (base.ATK * 0.1) ,
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF,
        "SPEED": base.SPEED+ (base.SPEED * 0.1) 
      }
    }else if (randomXingge === "急躁") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF- (base.DEF * 0.1),
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF,
        "SPEED": base.SPEED+ (base.SPEED * 0.1) 
      }
    }else if (randomXingge === "开朗") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK- (base.MATK * 0.1),
        "MDEF": base.MDEF,
        "SPEED": base.SPEED+ (base.SPEED * 0.1) 
      }
    }else if (randomXingge === "天真") {
      sortedStats = {
        "ATK": base.ATK ,
        "DEF": base.DEF,
        "HP": base.HP,
        "MATK": base.MATK,
        "MDEF": base.MDEF- (base.MDEF * 0.1),
        "SPEED": base.SPEED+ (base.SPEED * 0.1) 
      }
    }else{
      sortedStats = {
          "ATK": base.ATK ,
          "DEF": base.DEF,
          "HP": base.HP,
          "MATK": base.MATK,
          "MDEF": base.MDEF,
          "SPEED": base.SPEED
        }
    }
      
      return sortedStats;
}