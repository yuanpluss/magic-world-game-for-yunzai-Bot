export async function experience(level,exp){
    function calculateArray(num) {
        // 将数平均分成99份
        const average = num / 99;
        const arr = Array(99).fill(average);
        let a;
        let b = 99;
        for (let i = 0; i < arr.length; i++) {
          a = arr[i];
          if (i !== 98) {
            arr[i] = arr[i] / b;
            a = arr[i];
            arr[i + 1] = arr[i + 1] + a;
          }
          b--;
        }
      
        // 转换为整数
        for (let i = 0; i < arr.length; i++) {
          arr[i] = Math.floor(arr[i]);
        }
      
        return arr;
      }
      
      // 测试
      let num = 8000000; // 要分割的数
      let result = calculateArray(num);
      let needExp = [result[level-1]-exp,result[level-1]]//真实升到下一级所需经验和当前等级升到下一级所需全部经验
      return needExp
   

}